<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

class Rbac1Controller extends Controller
{
	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				
		
		$teammember = $auth->getRole('teammember');

		$updateOwnLead = $auth->getPermission('updateOwnLead');
		$auth->addChild($teammember, $updateOwnLead);
	
	}
}